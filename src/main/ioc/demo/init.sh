#!/bin/bash

caput SCANNINGTR:MSGS 	 INIT
sleep 0.2
# Configure pulse width on trigger line output 1
caput SCANNINGTR:TL3-WIDTH   5300
sleep 0.2
# Configure event 5 on sequence
caput SCANNINGTR:SEQ1 5
sleep 0.2
caput SCANNINGTR:SEQ1-NITER 1000000

sleep 0.2
# Turn timing receiver on - sequencer will automatically be turned on 
caput SCANNINGTR:MSGS         ON

sleep 0.2

caput -S SCANNINGTR:SEQ1-MSGS START

# Initialize Positioners in scan

caput SCANNING:WIRE1.P4SP -1
caput SCANNING:WIRE1.P4EP 1

caput SCANNING:WIRE1.P2SP 1
caput SCANNING:WIRE1.P2EP 1

caput SCANNING:WIRE1.NPTS 101
